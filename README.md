# mwtabR

R package to download and parse mwtab data from Metabolomics Workbench.
This package is new and likely has bugs due to unexpected structures of some mwtab files. Please report bugs.

It will pull a list of studies from MetabolomeXchange but only  Metabolomics Workbench studies are supported for pulling the data.

## Installation
Install the devtools package and run:

```r
install.packages("devtools")
devtools::install_git("https://gitlab.com/R_packages/mwtabR.git")
```


## Example

### Download a list of studies

```r
library(mwtabR)     # devtools::install_github("stanstrup/mwtabR")
data_table <- ME_get_studies_list()
```



### Filter Metabolomics Workbench 

```r
library(dplyr)
data_table_filtered <- data_table %>% filter(grepl("ST",accession))
```



### Download MW data

```r
MW_mwtab_parsed <- MW_get_studies_data(data_table_filtered[1:10,"accession"]) # here get just the first 10 studies
```



### View data
The returned list structure is rather complex and it can be visualized using the new package listviewer.

```r
library(listviewer) # devtools::install_github("timelyportfolio/listviewer")
jsonedit(MW_mwtab_parsed[1:2]) # Just the two first studies
```

This should give you something like this:
![Listviewer display for mwtab data](inst/extdata/mwtabR_listviewer_smaller.png)



### Access specific data
For example to access the list of metabolites for analysis AN000345 of study ST000231 you would do:

```r
MW_mwtab_parsed$ST000231$AN000345$METABOLITES
```
