% Please edit documentation in R/study_summary.R
\name{ME_get_studies_list}
\alias{ME_get_studies_list}
\title{Download list of studies from metabolomexchange}
\usage{
ME_get_studies_list()
}
\value{
A data.frame of study descriptions.
}
\description{
Download list of studies from metabolomexchange
}
\author{
Jan Stanstrup
}
