% Please edit documentation in R/study_get_and_parse.R
\name{MW_get_studies_data}
\alias{MW_get_studies_data}
\title{Get study data from Metabolomics Workbench}
\usage{
MW_get_studies_data(ids)
}
\arguments{
\item{ids}{A character vector of Metabolomics Workbench study ids}
}
\value{
A list of all the data for all the studies
}
\description{
Downloads and parses the mwtab data for each study
}
\author{
Jan Stanstrup
}
